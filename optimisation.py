import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from solar_production import *
from wind_production import *
from simulation import *
from dispatch import *
from opex import *
from capex import *
from pymoo.core.problem import ElementwiseProblem
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.factory import get_sampling, get_crossover, get_mutation
from pymoo.factory import get_termination
from pymoo.optimize import minimize
from pymoo.decomposition.asf import ASF
from pymoo.util.misc import stack


class MyProblem(ElementwiseProblem) :

    def __init__(self) :
        super().__init__(n_var = 5,
                         n_obj = 2,
                         n_constr = 0,
                         xl = np.zeros(5),
                         xu = np.array([5000,5000,50000,50000,50000]))

    weather = pd.read_csv('Ouessant_data_2016.csv')
    load = weather ['Load']


    def _evaluate(self, x, out, *args, **kwargs):

        weather = pd.read_csv('Ouessant_data_2016.csv')
        load = weather ['Load']
        P_solar_unit = solar_input()
        P_wind_unit = wind_input()

        (f1, h2) = simulation(dispatch = dispatch, load = load, P_solar_1kWp = P_solar_unit , P_wind_1kWp = P_wind_unit , x = x)
        f2 = 1 - h2

        out["F"] = [f1,f2]
        out["G"] = []




problem = MyProblem()

algorithm = NSGA2(
    pop_size=100,
    n_offsprings=20,
    sampling=get_sampling("real_random"),
    crossover=get_crossover("real_sbx", prob=0.9, eta=15),
    mutation=get_mutation("real_pm", eta=20),
    eliminate_duplicates=True
)


termination = get_termination("n_gen", 100)


res = minimize(problem,
               algorithm,
               termination,
               seed=1,
               save_history=True,
               verbose=True)

X = res.X
F = res.F
print(X)
print(F)

xl, xu = problem.bounds()
plt.figure(figsize=(7, 5))

plt.scatter(F[:, 0], F[:, 1], s=30, facecolors='none', edgecolors='blue')
plt.title("Objective Space")
plt.show()

approx_ideal = F.min(axis=0)
approx_nadir = F.max(axis=0)

plt.figure(figsize=(7, 5))
plt.scatter(F[:, 0], F[:, 1], s=30, facecolors='none', edgecolors='blue')
plt.scatter(approx_ideal[0], approx_ideal[1], facecolors='none', edgecolors='red', marker="*", s=100, label="Ideal Point (Approx)")
plt.scatter(approx_nadir[0], approx_nadir[1], facecolors='none', edgecolors='black', marker="p", s=100, label="Nadir Point (Approx)")
plt.title("Objective Space")
plt.legend()
plt.show()

nF = (F - approx_ideal) / (approx_nadir - approx_ideal)

fl = nF.min(axis=0)
fu = nF.max(axis=0)
print(f"Scale f1: [{fl[0]}, {fu[0]}]")
print(f"Scale f2: [{fl[1]}, {fu[1]}]")

plt.figure(figsize=(7, 5))
plt.scatter(nF[:, 0], nF[:, 1], s=30, facecolors='none', edgecolors='blue')
plt.title("Objective Space")
plt.show()


weights = np.array([0.2, 0.8])
decomp = ASF()
i = decomp.do(nF, 1/weights).argmin()

print("Best regarding ASF: Point \ni = %s\nF = %s\nX = %s" % (i, F[i],X[i]))

plt.figure(figsize=(7, 5))
plt.scatter(F[:, 0], F[:, 1], s=30, facecolors='none', edgecolors='blue')
plt.scatter(F[i, 0], F[i, 1], marker="x", color="red", s=200)
plt.title("Objective Space")
plt.show()