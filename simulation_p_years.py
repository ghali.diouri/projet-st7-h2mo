import pandas as pd
from solar_production import *
from wind_production import *
from simulation import *
from dispatch import *

weather = pd.read_csv('Ouessant_data_2016.csv')
load = weather ['Load']

x0 = [5.81326053e+02 , 3.41113700e+03 , 1.68289815e+01 ,  3.62733440e+04,  1.15406371e+03]

def simulation_simple (dispatch, load, P_solar_1kWp , P_wind_1kWp, x, Batt_level_initial, H2_level_initial, year):
    mat = np.zeros ((8759,5))
    P_solar = P_solar_1kWp * x[0]
    P_wind = P_wind_1kWp * x[1]
    Batt_level = Batt_level_initial
    H2_level = H2_level_initial
    mat[0,1] = Batt_level
    mat[0,3] = H2_level
    for i in range (1,8759):
        P_batt, Batt_level, P_H2, H2_level, P_delestage = dispatch(load = load[i] , P_solar = P_solar.iloc[i], P_wind = P_wind.iloc[i,0], Batt_max_level = x[2], H2_max_level = x[3], P_H2_max = x[4], Batt_level = mat[i-1,1], H2_level = mat[i-1,3])
        mat[i,0]= P_batt
        mat[i,1]= Batt_level
        mat[i,2]= P_H2
        mat[i,3]= H2_level
        mat[i,4]= P_delestage
    #Computing the final levels of Hydrogen and Battery
    Batt_final_level = mat[-1,1]
    H2_final_level = mat[-1,3]


    plt.plot(mat[:,1])
    plt.title('Battery level simulation ' + str(year + 1))

    plt.show()

    plt.plot(mat[:,3])
    plt.title('Hydrogen level simulation ' + str(year + 1))

    plt.show()

    return Batt_final_level, H2_final_level

weather = pd.read_csv('Ouessant_data_2016.csv')
load = weather ['Load']

def simulation_over_p_years(dispatch, load, x, p):

    Batt_level_initial = x[2] #on initialise à batt_max_level
    H2_level_initial = x[3]  #on initialise à H2_max_level
    print('Batt_level_initial = ' + str(Batt_level_initial))
    print('H2_level_initial = ' + str(H2_level_initial))
    for i in range(p):
        Batt_final_level, H2_final_level = simulation_simple(dispatch = dispatch, load = load , P_solar_1kWp = solar_input() , P_wind_1kWp = wind_input() , x = x, Batt_level_initial = Batt_level_initial, H2_level_initial = H2_level_initial, year = i)
        Batt_level_initial = Batt_final_level
        H2_level_initial = H2_final_level
        print('Batt_level_final après simulation ' + str(i+1) + ' = ' + str(Batt_level_initial))
        print('H2_level_final après simulation ' + str(i+1) + ' = ' + str(H2_level_initial))
    return None

simulation_over_p_years(dispatch = dispatch, load = load, x = x0, p = 5)
