import numpy as np
def generator_consumption (P_rated_gen, P_gen): 
    running_time = ((P_gen != 0)).sum()
    f0 = 0.05  #L/hr/kWrated
    f1 = 0.240 #L/hr/kWoutput
    return f0 * P_rated_gen* running_time + f1 * P_gen.sum(axis=0), running_time 

def cost_generator (P_rated_gen, P_gen):
    consumption, running_time = generator_consumption(P_rated_gen, P_gen)
    price_fuel = 1
    p_CAP_generator = 400 #$/kW
    fuel_cost = price_fuel * consumption
    lifetime_generator = 15000
    n = running_time // 15000
    CAPEX_generator = p_CAP_generator * (n+1)
    return CAPEX_generator + fuel_cost
