import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from dispatch import *
from opex import *
from capex import *
from generator import *
from time import perf_counter



def simulation (dispatch, load, P_solar_1kWp , P_wind_1kWp, x) :
    'x[0] = x_PV ;x[1] = x_W ;x[2]= x_B ;x[3] =x_H2 ;x[4] = x_H2_power'
    start = perf_counter()
    mat = np.zeros ((8759,5))
    P_solar = P_solar_1kWp * x[0]
    P_wind = P_wind_1kWp * x[1]
    Batt_level = x[2] #on initialise à batt_max_level
    H2_level = x[3]  #on initialise à H2_max_level
    mat[0,1] = Batt_level
    mat[0,3] = H2_level
    for i in range (1,8759):
        P_batt, Batt_level, P_H2, H2_level, P_delestage = dispatch(load = load[i] , P_solar = P_solar.iloc[i], P_wind = P_wind.iloc[i,0], Batt_max_level = x[2], H2_max_level = x[3], P_H2_max = x[4], Batt_level = mat[i-1,1], H2_level = mat[i-1,3])
        mat[i,0]= P_batt
        mat[i,1]= Batt_level
        mat[i,2]= P_H2
        mat[i,3]= H2_level
        mat[i,4]= P_delestage


    total_load = load.sum() #kWh
    total_failure = mat[:,4].sum() #kWh
    P_gen = mat[:,4]
    P_rated_gen_min = np.max(P_gen) #rated power minimale pour combler le delestage
    hours = ((P_gen != 0)).sum()
    p_OP_generator = 0.02 #$/kW/hour of operation

    service_rate = 1 - (total_failure/total_load)

    cost = opex (x[0],x[1],x[2],x[3], x[4]) + capex (x[0],x[1],x[2],x[3], x[4]) # + cost_generator (P_rated_gen_min, P_gen) + hours * p_OP_generator
    service_rate = 1 - (total_failure/total_load)
    lcoe = cost / (total_load*25)  #puisque la puissance delestage est comblée par un generateur dont les coûts sont pris en compte
    end = perf_counter()
    print('NPC = ',round(cost / 1000000,2),'M$' )
    print('LCOE = ', round (lcoe * 1000, 3),'$/MWh')
    print('Service Rate = ', round (service_rate *100,2),'%')

    K = 8000
    #fig, axs = plt.subplots(2,3)
    plt.plot(mat[:K,0])
    plt.title('Battery power')
    plt.show()
    #axs[0,0].set_title ('P batt')
    plt.plot(mat[:K,1])
    plt.title('Battery level')
    plt.show()
    #axs[0,1].set_title ('Batt level')
    plt.plot(mat[:K,2])
    plt.title('Hydrogen power')
    plt.show()
    #axs[0,2].set_title ('P H2')
    plt.plot(mat[:K,3])
    plt.title('Hydrogen level')
    plt.show()
    #axs[1,0].set_title ('H2 level')
    plt.plot(mat[:K,4])
    plt.title('Puissance de délestage')
    #axs[1,1].set_title ('P delestage')
    plt.show()

    print('simulation executée en',end - start,'s')

    return cost,service_rate
    #return cost, service_rate
