

def capex (x_PV,x_W,x_B,x_H2,x_H2_power):
    #p_CAP_generator = 400 $/kW
    p_CAP_solar = 1200 #$/kW
    p_CAP_wind = 1500 #$/kW
    p_CAP_batt = 350 #$/kWh
    p_CAP_h2_tank =  10 #$/kWh (avec la conversion L -> kWh pour H2)
    p_CAP_electrolyzer = 1000 #$/kW
    p_CAP_fc = 1000 #$/kW
    discount = 0.05

    lifetime_battery = 15 #years
    lifetime_project = 25 #years

    #CAPEX_generator = xG * p_CAP_generator
    CAPEX_solar = x_PV * p_CAP_solar
    CAPEX_wind = x_W * p_CAP_wind
    CAPEX_h2_tank = x_H2 * p_CAP_h2_tank
    CAPEX_electrolyzer = x_H2_power * p_CAP_electrolyzer
    CAPEX_fc = x_H2_power * p_CAP_fc
    CAPEX_battery = p_CAP_batt * x_B

    n_battery = lifetime_project // lifetime_battery
    remaining_battery_1 = lifetime_battery * (1- lifetime_project % lifetime_battery)
    negative_CAPEX_battery = (remaining_battery_1 / lifetime_battery) * p_CAP_batt * x_B
    
    #remplacement lorsque le lifetime est atteint
    CAPEX_battery += (n_battery)* p_CAP_batt * x_B + negative_CAPEX_battery * (1/((1+discount)**25))
    #n_generator = 'temps_util_gen' // lifetime_generator
    #CAPEX_generator+ = (n_generator) * p_CAP_generator 

    capex = CAPEX_solar + CAPEX_wind + CAPEX_battery + CAPEX_h2_tank + CAPEX_electrolyzer + CAPEX_fc
    return capex
