def opex (x_PV,x_W,x_B,x_H2,x_H2_power):
    p_OP_solar = 20 #$/kW/year
    p_OP_wind = 50 #$/kW/year
    p_OP_generator = 0.02 #$/kW/hour of operation
    p_OP_batt = 350 #$/kW/year
    p_OP_h2_tank = 10 #$/kWh/year
    P_OP_electrolyzer = 50 #$/kW/year
    P_OP_fc = 50 #$/kW/year
    #p_fuel = 1 #$/L
    discount = 0.05

    lifetime_project = 25

    #OPEX_generator = p_OP_generator * temps_util_gen + gen_consumption * p_fuel #temps_util_gen donné par le dispatch
    OPEX_solar = p_OP_solar * x_PV * (1-(1/(1+discount)**(lifetime_project))) /(1-(1/(1+discount)))
    OPEX_wind = p_OP_wind * x_W * (1-(1/(1+discount)**(lifetime_project))) /(1-(1/(1+discount)))
    OPEX_battery = p_OP_batt * x_B * (1-(1/(1+discount)**(lifetime_project))) /(1-(1/(1+discount)))
    OPEX_h2_tank =  p_OP_h2_tank * x_H2 * (1-(1/(1+discount)**(lifetime_project))) /(1-(1/(1+discount)))
    OPEX_electrolyzer = P_OP_electrolyzer * x_H2_power * (1-(1/(1+discount)**(lifetime_project))) /(1-(1/(1+discount)))
    OPEX_fc = P_OP_fc * x_H2_power * (1-(1/(1+discount)**(lifetime_project))) /(1-(1/(1+discount)))

    opex = OPEX_solar + OPEX_wind + OPEX_battery + OPEX_h2_tank + OPEX_electrolyzer + OPEX_fc

    return opex
