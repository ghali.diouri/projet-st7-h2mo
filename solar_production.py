import pandas as pd

data = pd.read_csv('Ouessant_data_2016_Ppv1k.csv')
#data = pd.DataFrame(data)
data = data.iloc[:,0]

def solar_input(): 
    return data / 1000 #on divise par 1000 pour se mettre dans la bonne unité

#solar_kWp = solar_input

def solar_production (x_PV, solar_kWp):
    'xPV is the PV sizing of the microgrid in kWp'
    'solar_kWP is the production power for a 1kWp solar PV array at our location'
    return x_PV * solar_kWp

