import numpy as np
import pandas as pd

data = pd.read_csv('Ouessant_data_2016.csv')
data = pd.DataFrame(data)

wind_parameters = pd.read_excel('Ouessant_params.xlsx',"Wind power curve")
wind_parameters.columns = wind_parameters.iloc[0]
wind_parameters = wind_parameters.drop(2)
wind_parameters = wind_parameters.drop(17) #sinon on a 2 valeurs pour 4m/s
wind_parameters = wind_parameters [1:]

def wind_production (speed) :
    if speed <= 25 and speed >=4 :
        i = np.argmin(abs (speed - wind_parameters['speed (m/s)']))
        return float(wind_parameters['Power (kW)'].iloc[i])
    elif speed < 4 :
        return 0.0
    elif speed > 25 : 
        return 0.0

def wind_input() : 
    wind_prod = data ['Wind'].transform(wind_production) #production point par point pour une éolienne de 1500 kW
    wind_prod = pd.DataFrame(wind_prod)
    #wind_prod.columns = ['Wind power (kW)']
    return wind_prod / 1500

#wind_prod = wind_input()


def wind_turbines_production (x_W, wind_prod ):
    'x_W is the wind turbine sizing of the microgrid'
    'wind_prod is the power equivalent of wind speed for a 80m high turbine with rated power P = 1500 kW'
    return x_W * wind_prod
