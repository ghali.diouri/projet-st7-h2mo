import pandas as pd
from solar_production import *
from wind_production import *
from simulation import *
from dispatch import *
from time import perf_counter



start = perf_counter()

weather = pd.read_csv('Ouessant_data_2016.csv')
load = weather ['Load']
#load = pd.read_csv('Ouessant_data_2016_Load.csv')
#x[0] = x_PV ;x[1] = x_W ;x[2]= x_B ;x[3] =x_H2 ;x[4] = x_H2_power'
#x0 = [9.54100687e+03, 1500, 8.97234359e+02, 5000, 8.44381109e+02]
#x0 = [1.72181965e+03, 2.06861095e+03, 2.17153316e+00, 1.28022604e+04, 6.95930656e+02] #93.3% et 11.66M$ sans générateur
x0 = [2.20967392e+03 , 3.63786112e+03 , 9.06610822e+01 ,  4.10962197e+04,  1.47685346e+03] # 99.91% et 23.53M$ sans générateur

print( simulation (dispatch = dispatch, load = load , P_solar_1kWp = solar_input() , P_wind_1kWp = wind_input() , x = x0 ))

end = perf_counter()

print ('main.py exécuté en ',end-start,'s')
