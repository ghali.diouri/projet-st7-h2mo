import numpy as np
from pymoo.core.problem import ElementwiseProblem
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.factory import get_sampling, get_crossover, get_mutation
from pymoo.factory import get_termination
from pymoo.optimize import minimize
import matplotlib.pyplot as plt
from pymoo.decomposition.asf import ASF

class MyProblem(ElementwiseProblem):

    def __init__(self):
        super().__init__(n_var=6,
                         n_obj=2,
                         n_constr=6,
                         xl=np.array([-2,-2,-2, -2, -2, -2]),
                         xu=np.array([2,2, 2, 2, 2, 2]))

    def _evaluate(self, x, out, *args, **kwargs):
        f1 = -(25*(x[0]-2)**2+(x[1]-2)**2+(x[2]-1)**2+(x[3]-4)**2+(x[4]-1)**2)
        f2 = x[0]+x[1]+x[2]+x[3]+x[4]+x[5]

        g1 = 2-x[0]-x[1]
        g2 = x[0]+x[1]-6
        g3 = x[1]-x[0]-2
        g4 = x[0]-3*x[1]-2
        g5 = x[3]+(x[2]-3)**2-4
        g6 = 4-x[5]-(x[4]-3)**2

        out["F"] = [f1, f2]
        out["G"] = [g1, g2,g3,g4,g5,g6]


problem = MyProblem()



algorithm = NSGA2(
    pop_size=100,
    n_offsprings=20,
    sampling=get_sampling("real_random"),
    crossover=get_crossover("real_sbx", prob=0.9, eta=15),
    mutation=get_mutation("real_pm", eta=20),
    eliminate_duplicates=True
)


termination = get_termination("n_gen", 100)




res = minimize(problem,
               algorithm,
               termination,
               seed=1,
               save_history=True,
               verbose=True)

X = res.X
F = res.F



F = res.F
xl, xu = problem.bounds()
plt.figure(figsize=(7, 5))
plt.scatter(F[:, 0], F[:, 1], s=30, facecolors='none', edgecolors='blue')
plt.title("Objective Space")
plt.show()

approx_ideal = F.min(axis=0)
approx_nadir = F.max(axis=0)

plt.figure(figsize=(7, 5))
plt.scatter(F[:, 0], F[:, 1], s=30, facecolors='none', edgecolors='blue')
plt.scatter(approx_ideal[0], approx_ideal[1], facecolors='none', edgecolors='red', marker="*", s=100, label="Ideal Point (Approx)")
plt.scatter(approx_nadir[0], approx_nadir[1], facecolors='none', edgecolors='black', marker="p", s=100, label="Nadir Point (Approx)")
plt.title("Objective Space")
plt.legend()
plt.show()

nF = (F - approx_ideal) / (approx_nadir - approx_ideal)

fl = nF.min(axis=0)
fu = nF.max(axis=0)
print(f"Scale f1: [{fl[0]}, {fu[0]}]")
print(f"Scale f2: [{fl[1]}, {fu[1]}]")

plt.figure(figsize=(7, 5))
plt.scatter(nF[:, 0], nF[:, 1], s=30, facecolors='none', edgecolors='blue')
plt.title("Objective Space")
plt.show()


weights = np.array([0.2, 0.8])
decomp = ASF()
i = decomp.do(nF, 1/weights).argmin()

print("Best regarding ASF: Point \ni = %s\nF = %s" % (i, F[i]))

plt.figure(figsize=(7, 5))
plt.scatter(F[:, 0], F[:, 1], s=30, facecolors='none', edgecolors='blue')
plt.scatter(F[i, 0], F[i, 1], marker="x", color="red", s=200)
plt.title("Objective Space")
plt.show()