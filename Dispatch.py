import numpy as np

def dispatch(Batt_max_level, load, P_solar, P_wind, H2_max_level, P_H2_max, Batt_level, H2_level):

    net_load = load - P_solar - P_wind
    timestep = 1
    Batt_ratio = 1
    Batt_min_level = Batt_max_level/10
    P_batt_in = 0
    P_batt_out = 0
    P_H2_in = 0
    P_H2_out = 0
    P_delestage = 0
    rendement_H2_in = 0.6
    rendement_H2_out = 0.5

    P_batt_max = Batt_max_level / Batt_ratio

    if net_load < 0.0 :  #Surplus d'énergie renouvelable

        P_batt_max_in = min(P_batt_max, (Batt_max_level - Batt_level)/timestep)
        P_batt_in = min(-net_load, P_batt_max_in)
        Batt_level += P_batt_in * timestep

        if P_batt_in == P_batt_max_in :

            P_missing = -net_load - P_batt_in  #On ne peut pas charger plus la batterie, la puissance qui reste charge l'hydrogène
            P_H2_in = min(P_missing, min(P_H2_max, (H2_max_level - H2_level)/(rendement_H2_in * timestep)))
            H2_level += rendement_H2_in * P_H2_in * timestep



    if net_load.item() > 0. :  #manque d'énergie renouvelable

        P_batt_max_out = min(P_batt_max, (Batt_level - Batt_min_level)/timestep)
        P_batt_out = min(P_batt_max_out, net_load)
        Batt_level += -P_batt_out * timestep

        if P_batt_out == P_batt_max_out :

            P_missing = net_load - P_batt_out #le manque de puissance à compléter avec le générateur à hydrogène
            P_H2_out = np.minimum(P_missing, np.minimum(P_H2_max, H2_level/(rendement_H2_out * timestep)))
            H2_level += -rendement_H2_out * P_H2_out * timestep

            if P_H2_out < P_missing :
                P_delestage = P_missing - P_H2_out

    #Les puissances sont calculées en convention récepteur
    P_batt = P_batt_in - P_batt_out
    P_H2 = P_H2_in - P_H2_out

    return P_batt, Batt_level, P_H2, H2_level, P_delestage